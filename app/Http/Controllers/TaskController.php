<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index()
    {
        $authUser = Auth::user();
        $items = Task::with('user')->get();

        $params = [
            'authUser' => $authUser,
            'items' => $items,
        ];
        return view('task.index', $params);
    }

    public function store(Request $request)
    {
        $task = new task;
        $form = $request->all();

        $rules = [
            'user_id' => 'integer|required', // 2項目以上条件がある場合は「 | 」を挟む
        ];
        $message = [
            'user_id.integer' => 'System Error',
            'user_id.required' => 'System Error',
        ];
        $validator = Validator::make($form, $rules, $message);

        if($validator->fails()){
            return redirect('/task')
                ->withErrors($validator)
                ->withInput();
        }else{
            unset($form['_token']);
            $task->user_id = $request->user_id;
            $task->task_name = $request->task_name;
            $task->task_content = $request->task_content;
            $task->progress = $request->progress;
            $task->save();
            return redirect('/task');
        }
    }

    public function show($id)
    {
        $authUser = Auth::user();
        $item = Task::find($id);

        if($item->progress === '1') {
            $progress = '未着手';
            $color = 'orange';
            $background = 'orange-back';
        }
        if($item->progress === '2') {
            $progress = '進行中';
            $color = 'green';
            $background = 'green-back';
        }
        if($item->progress === '3') {
            $progress = '完了';
            $color = 'blue';
            $background = 'blue-back';
        }

        $params = [
            'authUser' => $authUser,
            'item' => $item,
            'progress' => $progress,
            'color' => $color,
            'background' => $background,
        ];
        return view('task.show', $params);
    }


    public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $form = $request->all();

        unset($form['_token']);
        $task->user_id = $request->user_id;
        $task->task_name = $request->task_name;
        $task->task_content = $request->task_content;
        $task->progress = $request->progress;
        $task->save();
        return redirect('/task');
    }

    public function destroy($id)
    {
        $items = Task::find($id)->delete();
        return redirect('/task');
    }
}