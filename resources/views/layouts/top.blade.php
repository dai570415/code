<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/top.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container">
            
            <div class="contents">
                @yield('content')
            </div>

        </div>

        <!-- 以下JS -->
        <p id="page-top"><a href="">↑</a></p>
        <div id="stalker"></div>
        <script>
            const stalker = document.getElementById('stalker'); 
            document.addEventListener('mousemove', function (e) {
                stalker.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
            });

            let x = document.getElementById('login');
            let y = document.getElementById('register');
            let z = document.getElementById('btn');

            function register() {
                x.style.left = '-400px';
                y.style.left = '50px';
                z.style.left = '110px';
            }
            function login() {
                x.style.left = '50px';
                y.style.left = '450px';
                z.style.left = '0';
            }

            $(function(){
                // fadein
                $(window).scroll(function (){
                    $('.fadein').each(function(){
                        let targetElement = $(this).offset().top;
                        let scroll = $(window).scrollTop();
                        let windowHeight = $(window).height();
                        if (scroll > targetElement - windowHeight + 200){
                            $(this).css('opacity','1');
                            $(this).css('transform','translateY(0)');
                        }
                    });
                });

                // ページTOPに戻る
                let topBtn = $('#page-top');    
                topBtn.hide();

                // スクロールダウン文言
                let scrollDown = $('.scroll-down');
                scrollDown.show();
                
                //スクロールが100に達したらボタン表示
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 100) {
                        topBtn.fadeIn();
                        scrollDown.fadeOut();
                    } else {
                        topBtn.fadeOut();
                        scrollDown.fadeIn();
                    }
                });

                //スクロールしてトップ
                topBtn.click(function () {
                    $('body,html').animate({
                        scrollTop: 0
                    }, 500);
                    return false;
                });

                // モーダル
                $('.task-modal-open').on('click',function(){
                    $('.task-modal').fadeIn();
                    return false;
                });
                $('.task-modal-close').on('click',function(){
                    $('.task-modal').fadeOut();
                    return false;
                });
                $('.task-modal-close-icon').on('click',function(){
                    $('.task-modal').fadeOut();
                    return false;
                });
            });
        </script>
    </div>
</body>
</html>
