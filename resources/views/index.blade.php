@extends('layouts.top')

@section('content')

@guest
    <div class="content">
        <div class="main-title">
            <h1>毎日の仕事を<br />もっとスマートに</h1>
            <p>進捗状況が丸わかり! タスク管理ツール</p>
        </div>

        <div class="main-form">
        <img src="{{ asset('img/illust1.png') }}" class="illust1" alt="illust">
            <div class="form-box">
                <div class="button-box">
                    <div id="btn"></div>
                    <button type="button" class="toggle-btn" onclick="login()">ログイン</button>
                    <button type="button" class="toggle-btn" onclick="register()">無料登録</button>
                </div>
                <form method="post" action="{{ route('login') }}" class="input-group" id="login">
                    <div class="explanation-text">登録したアカウントでログインしてください</div>
                    @csrf
                    <input type="email" class="input-field" name="email" value="{{ old('email') }}" placeholder="メールアドレス" required>
                    @error('email')
                        {{ $message }}
                    @enderror
                    <input type="password" class="input-field" name="password" value="{{ old('email') }}" placeholder="パスワード" required>
                    @error('password')
                        {{ $message }}
                    @enderror
                    <button type="submit" class="submit-btn">ログインする</button>
                </form>
                <form method="POST" action="{{ route('register') }}" class="input-group" id="register">
                    <div class="explanation-text">アカウント登録してください</div>
                    @csrf
                    <input type="text" class="input-field" name="name" value="{{ old('name') }}" placeholder="ユーザー名" required>
                    @error('name')
                        {{ $message }}
                    @enderror
                    <input type="email" class="input-field" name="email" value="{{ old('name') }}" placeholder="メールアドレス" required>
                    @error('email')
                        {{ $message }}
                    @enderror
                    <input type="password" class="input-field" name="password" placeholder="パスワード" required>
                    @error('password')
                        {{ $message }}
                    @enderror
                    <input type="password" class="input-field" name="password_confirmation" placeholder="もう一度パスワード入力" required>
                    <button type="submit" class="submit-btn">無料登録する</button>
                </form>
            </div>
        </div>
    </div>
@endguest

@endsection
