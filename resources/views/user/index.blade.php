@extends('layouts.app')
@section('title','ユーザー情報')
@section('content')

<div class="user-content">
  @if(!empty($authUser->thumbnail))
    <img src="/storage/user/{{ $authUser->thumbnail }}" class="thumbnail">
  @else
    <div class="thumbnail"></div>
  @endif
  <div class="user-name">
    {{ $authUser->name }}
  </div>
  <div class="user-email">
    {{ $authUser->email }}
  </div>
  {{--<!-- <a href="" class=""></a>

  <form method="post" action="{{ route('user.userUpdate') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="user_id" value="{{ $authUser->id }}">
    @if($errors->has('user_id'))<div class="error">{{ $errors->first('user_id') }}</div>@endif
    <input type="text" class="userForm" name="name" placeholder="User" value="{{ $authUser->name }}">
    @if($errors->has('name'))<div class="error">{{ $errors->first('name') }}</div>@endif
    <input type="file" name="thumbnail">
    <input type="submit" name="send" value="ユーザー変更" class="">
  </form> -->--}}
</div>

@endsection