@extends('layouts.app')
@section('title', 'タスク管理アプリ')

@section('content')
    <div class="modal task-modal">
        <div class="modal__bg task-modal-close"></div>
        <div class="modal__content">
            <img src="{{ asset('img/closeIcon_black.svg') }}" class="task-modal-close-icon">
            <!-- 投稿フォーム -->
            <form action="/task" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="user_id" value="{{ $authUser->id }}">
                <div class="form-label">ToDoタイトル</div>
                <input type="text" class="task-form" name="task_name" placeholder="ToDoタイトル" value="{{ old('task_name') }}">
                <div class="form-label">ToDo内容</div>
                <textarea class="task-textarea" name="task_content" placeholder="詳しく記入">{{ old('task_content') }}</textarea>
                <input type="hidden" class="task-form" name="progress" placeholder="進捗フラグ" value="1">
                <input type="submit" class="create task-button" value="ToDoを追加する">
            </form>
        </div>
    </div>

    <div class="content">
        <!-- 1列め $item->progress == '1' -->
        <div class="content-box">
            <div class="state orange-back">未着手<a href="" class="task-modal-open">+</a></div>
            @if(count($items) > 0)
                @foreach($items as $item)
                    @if($authUser->id === $item->user_id && $item->progress == '1')
                        <div class="content-item orange">
                            <div class="inner-content-item">
                                <div class="title-name">
                                    <a href="/task/{{ $item->id }}">
                                        <span class="id"># {{ $item->id }} - </span>
                                        {{ $item->task_name }}
                                    </a>
                                </div>
                                <p class="break">{{ $item->task_content }}</p>
                                <div class="action">
                                    <div class="left-action">
                                        <a href="/task/{{ $item->id }}"><i class="fas fa-edit"></i></a>
                                        <form action="/task/{{ $item->id }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="delete">
                                            <i class="far fa-calendar-times"></i>
                                        </button>
                                        </form>
                                        <span class="datetime">{{ $item->updated_at }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
 
        <!-- 2列め $item->progress == '2' -->
        <div class="content-box">
            <div class="state green-back">進行中</div>
            @if(count($items) > 0)
                @foreach($items as $item)
                    @if($authUser->id === $item->user_id && $item->progress == '2')
                        <div class="content-item green">
                            <div class="inner-content-item">
                                <div class="title-name">
                                    <a href="/task/{{ $item->id }}">
                                        <span class="id"># {{ $item->id }} - </span>
                                        {{ $item->task_name }}
                                    </a>
                                </div>
                                <p class="break">{{ $item->task_content }}</p>
                                <div class="action">
                                    <div class="left-action">
                                        <a href="/task/{{ $item->id }}"><i class="fas fa-edit"></i></a>
                                        <form action="/task/{{ $item->id }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="delete">
                                            <i class="far fa-calendar-times"></i>
                                        </button>
                                        </form>
                                        <span class="datetime">{{ $item->updated_at }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>

        <!-- 3列め $item->progress == '3' -->
        <div class="content-box">
            <div class="state blue-back">完了</div>
            @if(count($items) > 0)
                @foreach($items as $item)
                    @if($authUser->id === $item->user_id && $item->progress == '3')
                        <div class="content-item blue">
                            <div class="inner-content-item">
                                <div class="title-name">
                                    <a href="/task/{{ $item->id }}">
                                        <span class="id"># {{ $item->id }} - </span>
                                        {{ $item->task_name }}
                                    </a>
                                </div>
                                <p class="break">{{ $item->task_content }}</p>
                                <div class="action">
                                    <div class="left-action">
                                        <a href="/task/{{ $item->id }}"><i class="fas fa-edit"></i></a>
                                        <form action="/task/{{ $item->id }}" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit" class="delete">
                                            <i class="far fa-calendar-times"></i>
                                        </button>
                                        </form>
                                        <span class="datetime">{{ $item->updated_at }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>

    </div>
@endsection