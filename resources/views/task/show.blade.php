@extends('layouts.app')
@section('title', 'タスク詳細')

@section('content')
    <div class="content content-show">

        @if($item !== '')
        <div class="content-box">
            <div class="show-form">
                <form action="/task/{{$item->id}}" method="POST">
                    {{ csrf_field() }}           
                    <input type="hidden" name="user_id" value="{{ $authUser->id }}">
                    <input type="text" class="task-form" name="task_name" placeholder="やることリスト" value="{{ $item->task_name }}">
                    <textarea class="task-textarea" name="task_content" placeholder="詳しくやることを記入">{{ $item->task_content }}</textarea>

                    <input type="radio" id="not-yet" name="progress" value="1" class="radio-inline__input" <?= $item->progress === '1' ? 'checked' : ''; ?> >
                    <label for="not-yet" class="radio-inline__label">未着手</label>
                    <input type="radio" id="in-progress" name="progress" value="2" class="radio-inline__input" <?= $item->progress === '2' ? 'checked' : ''; ?> >
                    <label for="in-progress" class="radio-inline__label">進行中</label>
                    <input type="radio" id="done" name="progress" value="3" class="radio-inline__input" <?= $item->progress === '3' ? 'checked' : ''; ?> >
                    <label for="done" class="radio-inline__label">完了</label>
                    
                    <input type="hidden" name="_method" value="PUT">
                    <input type="submit" class="create task-button {{ $background }}" value="変更">
                    <a href="/task" class="prev">一覧へ戻る</a>
                </form>
            </div>
        </div>
        @endif


        {{--<!-- <div class="content-box">
            <div class="state {{ $background }}">{{ $progress }}</div>
            @if($item !== '')
                @if($authUser->id === $item->user_id)
                    <div class="content-item {{ $color }}">
                        <div class="inner-content-item">
                            <div class="title-name">
                                    <span class="id"># {{ $item->id }} - </span>
                                    {{ $item->task_name }}
                            </div>
                            <p class="break">{{ $item->task_content }}</p>
                            <div class="action">
                                <div class="left-action">
                                    <span class="datetime">{{ $item->updated_at }}</span>
                                    <form action="/task/{{ $item->id }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="delete">
                                        <i class="far fa-calendar-times"></i>
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div> -->--}}

    </div>
    
@endsection